<h1>Este é o Teste Final do Pedro</h1>
<h2>Após apresentação da documentação e auxílios internos</h2>

<p>Este pequeno sistema tem como finalidade um CRUD básico de produtos com nome, descrição, preço e imagem</p>

<p>O banco de dados vem sem dados de produtos</p>

<p>Para utilizá-lo siga os passos:
<ol>
    <li>$ git clone git@bitbucket.org:pedroida/teste_lets.git</li>
    <li>$ composer install</li>
    <li>$ php artisan migrate</li>
    <li>$ php artisan serve</li>
</ol>
