@extends('layouts.app')

@section('title','Início')
@section('content')

	<div class="row text-center panel">
		<div class="page-header">
			<h1>Lista de Produtos</h1>
		</div>
			<div class="alert ">
				<a class="btn btn-success col-md-2" href="{{ route('products.create') }}" role="button" >Adicionar produto</a>
			</div>
			<div class="panel-body col-md-12">
			@foreach($products as $product)
			<div class="col-md-2 col-lg-3  card" >
				<img id="productImage" src="{{$product->image }}" alt="{{$product->image}}">
				<div class="in-card caption well">
					<h4><b>Título:</b><br>
						<div id="titulo">
							{{ $product->limitedTitleIn(20) }}
						</div>
					</h4>
					<span class="badge">{{  'R$ '.number_format($product->price, 2, ',', '.') }} </span>
				</div>
				<div class="btn-card">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{ $product->id}}">
						Visualizar
					</button>
					<a href="{{ route('products.edit', [$product->id]) }}"  class="btn btn-warning">Editar Produto</a>

				</div>
			</div>


			<!-- Modal -->
			<div class="modal fade" id="{{ $product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Visualização Completa</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="thumbnail">
								<img id="showImage" class="img-responsive" src="{{asset($product->image) }}" alt="{{$product->image}}">
								<div class="caption well">
									<h3><b>ID:</b> {{ $product->id }}</h3>
									<h3><b>Título:</b> {{ $product->title }}</h3>
									<h3><b>Descrição:</b> {{ $product->description }}</h3>
									<span class="badge">{{  'R$ '.number_format($product->price, 2, ',', '.') }} </span>
								</div>
								<a class="btn btn-info" href="{{ route('products.edit',$product->id) }}">Editar</a>

								<a href="{{ route('products.delete',[$product->id]) }}"  class="btn btn-danger" >Apagar</a>

							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>






@stop()
