@extends('layouts.app')

@section('title','Cadastrar')
@section('content')


	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="panel panel-danger">
				@else
					<div class="panel panel-success">
				@endif
				<div class="panel-heading">
					Cadastrar Produto
				</div>

				<div class="panel-body well">
					@include('layouts.partials.form-errors')
					<form action="{{ route('products.create') }}" method="post" enctype="multipart/form-data">
						@include('products.form')
					</form>
				</div>
			</div>
		</div>
	</div>
@stop()
