{{-- campos que forem se repetir, fica bom usar com o @include desse tipo --}}

{{-- @include('products.field', ['label' => 'Título', 'name' => 'title', 'value' => $product->title  ?? '']) --}}

<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input class="form-control" type="text" name="{{ $name }}" value="{{ old($name) ?? $value}}">
</div>
