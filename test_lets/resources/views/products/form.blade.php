@if(isset($product) && $product->image)
    <div class="text-center">
        <img src="{{ $product->image }}" alt="" style="maxWidth: 30vw; maxHeight:30vh;">
    </div>
@endif
<div class="form-group">
    {{ Form::label('title', 'Título') }}
    {{ Form::text('title', $product->title ?? '', ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Descrição') }}
    {{ Form::text('description', $product->description ?? '', ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('price', 'Preço (R$)') }}
    {{ Form::text('price', $product->price ?? '', ['class' => 'form-control preco']) }}
</div>
<div class="form-group">
    {{ Form::label('image', 'Imagem') }}
    {{ Form::file('image', $attributes = ['class' => 'form-control-file']) }}
</div>
{{ Form::submit('Salvar', $attributes = ['class' => 'btn btn-success']) }}
@if(isset($product))
    {{ link_to_route('products.delete', $title = 'Excluir', $parameters = ['id' => $product->id], $attributes = ['class' => 'btn btn-danger']) }}
@endif
{{ link_to_route('products.index', 'Voltar', $parameters = [], $attributes = ['class' => 'btn btn-default']) }}
{{ Form::token() }}
