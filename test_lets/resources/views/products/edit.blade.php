@extends('layouts.app')

@section('title','Editar')
@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="panel panel-danger">
				@else
				<div class="panel panel-success">
				@endif
				<div class="panel-heading">
					Editar {{ $product->limitedTitleIn(20) }}({{ $product->id }})
				</div>

				<div class="panel-body">
					@include('layouts.partials.form-errors')
					<form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
						<input name="_method" type="hidden" value="PUT">
						@include('products.form')
					</form>
				</div>
			</div>
		</div>
	</div>

@stop()
