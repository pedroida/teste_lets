@extends('layouts.app')

@section('title', 'Perfil')

@section('content')

<div class="row text-center">
    <div class="col-md-8 col-md-offset-2">
        <div class="thumbnail">
            <div class="panel-heading">
                <h1>Perfil</h1>
            </div>
            <div class="panel-body">
                <img class="img-circle" src="{{ $user->pic }}" alt="" style="width:20vh;">
                <h1>{{ $user->name }}</h1>
                <h1>{{ $user->email }}</h1>
            </div>
        </div>
    </div>
</div>

@stop()
