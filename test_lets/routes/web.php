<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
	Route::get('/index', 'ProductController@showIndex')->name('products.index');
	Route::get('visualizar/{id}', 'ProductController@showItem')->name('products.show');
	Route::get('/cadastrar', 'ProductController@register')->name('products.create');
	Route::post('/cadastrar', 'ProductController@save')->name('products.store');
	Route::get('/editar/{id}', 'ProductController@edit')->name('products.edit');
	Route::put('/editar/{id}', 'ProductController@update')->name('products.update');
	Route::get('/excluir/{id}', 'ProductController@delete')->name('products.delete');
	Route::get('perfil/{id}', 'UserController@showProfile')->name('user.profile');
	Route::get('/error', 'LoginController@error');
});
