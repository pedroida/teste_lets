let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/masks.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
  'public/js/app.js',
  'resources/assets/js/jquery.maskMoney.min.js',
  'public/js/masks.js',
], 'public/js/app.js');

mix.styles([
    'public/css/app.css',
    'resources/assets/css/custom.css',
], 'public/css/app.css');
