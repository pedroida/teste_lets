<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['title', 'description', 'price'];


    public function getImageAttribute($image)
    {
    	return "/storage/{$image}";
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = ucfirst($name);
    }

     public function setDescriptionAttribute($description)
    {
        $this->attributes['description'] = ucfirst($description);
    }

    public function setPriceAttribute($price)
    {
    	$source = array('.', ',');
        $replace = array('', '.');
        $price = str_replace($source, $replace, $price);
        $this->attributes['price'] = $price;
    }

    public function limitedTitleIn($characteres)
    {
        return str_limit($this->title,$characteres);
    }
}
