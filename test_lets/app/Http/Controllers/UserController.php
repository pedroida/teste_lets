<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function showProfile(Request $request)
    {
        $user = $request->user();

        return view('user.profile', compact('user'));
    }
}
