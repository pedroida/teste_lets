<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Product;
use App\Http\Requests\ProductsRequest;

class ProductController extends Controller
{
    public function showIndex(Request $request)
    {
        $query = $request->get('query');
        if ($query) {
            $products = Product::where('title', 'like', '%'.$query.'%')
                              ->orWhere('description', 'like', '%'.$query.'%')
                              ->get();
        } else {
            $products = Product::all();
        }

        return view('products.index', compact('products'));
    }

    public function showItem($id)
    {
        $product = Product::find($id);

        return view('products/show', compact('product'));
    }

    public function register()
    {
        return view('products/create');
    }

    public function save(ProductsRequest $request)
    {
        $data = $request->all();
        $product = Product::create($data);

        $image = $data['image'];
        $imageURL = Storage::disk('public')->put("products/{$product->id}", $image);

        $product->image = $imageURL;
        $product->save();

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return view('products.edit')->with('product', $product);
    }

    public function update(ProductsRequest $request, $id)
    {
        $data = $request->all();

        $product = Product::find($id);

        if(isset($data['image'])){
            $data['image'] = Storage::disk('public')->put("products/{$id}", $data['image']);
            $product->image = $data['image'];
        }

        $product->fill($data);
        $product->update();

        return redirect()->route('products.show', [$id]);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();

        $productDirectory = "products/{$id}";
        Storage::disk('public')->deleteDirectory($productDirectory);

        return redirect()->route('products.index');
    }
}
